module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        green: {
          DEFAULT: '#4BB967',
          50: '#CFECD7',
          100: '#C1E7CA',
          200: '#A3DBB1',
          300: '#86D099',
          400: '#68C480',
          500: '#4BB967',
          600: '#399350',
          700: '#296A3A',
          800: '#1A4224',
          900: '#0A1A0E',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
